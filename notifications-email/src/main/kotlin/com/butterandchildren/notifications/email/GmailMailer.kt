package com.butterandchildren.notifications.email

import org.apache.commons.mail.DefaultAuthenticator
import org.apache.commons.mail.SimpleEmail

object GmailMailer {
    private val emailFromAddress = System.getenv("EMAIL_ADDRESS")
    private val emailFromPassword = System.getenv("EMAIL_PASSWORD")
    private val emailFrom = System.getenv("EMAIL_FROM")

    suspend fun sendEmail(emailNotification: EmailNotification){
        SimpleEmail().apply {
            hostName = "smtp.googlemail.com"
            setSmtpPort(465)
            setAuthenticator(DefaultAuthenticator(emailFromAddress, emailFromPassword))
            isSSLOnConnect = true
            setFrom(emailFrom)
            subject = emailNotification.subject
            setMsg(emailNotification.message)
            addTo(emailNotification.destination)
            send()
        }
    }
}