package com.butterandchildren.notifications.email

data class EmailNotification(
    val subject: String,
    val message: String,
    val destination: String
)
