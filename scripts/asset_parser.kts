import java.io.File

val sideUP = "UP"
val sideDown = "DOWN"

val back = "https://ultimate-board-game.herokuapp.com/static/purple_back.jpg"

val beginAssets = """
    {
        "assets": [
""".trimIndent()

val endAssets = """
        ]
    }
""".trimIndent()

val beginEachAsset = """
    {
      "id": "%s",
      "name": "%s asset name",
      "description": "%s asset name card collection (shuffled)",
      "maxBoardZones": 10,
      "maxHandZones": 10,
      "startsShuffled": true,
      "zones": [
        {"id": "asset_deck_%s",
          "dimension": {
            "width": 1,
            "height": 1
          },
          "objects": [
""".trimIndent()

val endEachAsset = """
                    ]
                }
            ]
        },
""".trimIndent()

val eachObject = """
        {
            "id": "card__%s",
            "type": "CARD",
            "side": "$sideDown",
            "imageUp": "https://ultimate-board-game.herokuapp.com/static/%s",
            "imageDown": "%s"
        },
""".trimIndent()

var finalAsset = beginAssets
val objects = mutableListOf<String>()

File("../rest/resources/cards").walk().forEach {

    if (it.isDirectory && it.path != "../rest/resources/cards") {
        // a directory
        if (!objects.isEmpty()) {
            finalAsset += endEachAsset
            objects.clear()
        }
        val name = it.path.replace("/", "_").replace(".._rest_resources_", "")
        finalAsset += String.format(beginEachAsset, name, name, name, name)
        objects.clear()
    } else if (it.path.contains(".jpg") && !it.path.contains("purple_back.jpg")) {
        objects.add(it.path)
        val arrayPath = it.path.split("/")
        val name = arrayPath[arrayPath.size - 1]
        finalAsset += String.format(eachObject, name.replace("-", "__").replace(".jpg", ""), name, back)
    }
}

finalAsset += endEachAsset
finalAsset += endAssets
finalAsset = finalAsset.replace("},.*]".toRegex(), "}]")

println(finalAsset)
