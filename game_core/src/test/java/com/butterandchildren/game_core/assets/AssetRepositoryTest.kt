package com.butterandchildren.game_core.assets

import org.junit.Assert.assertEquals
import org.junit.Test

internal class AssetRepositoryTest {

    @Test
    fun all() {
        assertEquals(4, AssetRepository.all().size)
    }
}
