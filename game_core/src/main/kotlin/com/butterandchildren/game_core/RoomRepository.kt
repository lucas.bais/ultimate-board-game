package com.butterandchildren.game_core

import com.butterandchildren.game_core.game.Guest
import com.butterandchildren.game_core.game.Room
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicInteger

object RoomRepository {

    private val roomId = AtomicInteger()
    private val rooms = ConcurrentHashMap<String, Room>()

    // TODO add room creation restrictions.
    fun createRoom(ownerId: Int, name: String? = null, password: String? = null): Room {
        val roomId = "${roomId.getAndIncrement()}"
        val room = Room(ownerId, roomId, name = name ?: "Room - $roomId", password = password)
        rooms[roomId] = room
        return room
    }

    fun getRoom(roomId: String) = rooms[roomId] ?: run {
        throw IllegalStateException("No roomId found $roomId")
    }

    fun join(roomId: String, guest: Guest, password: String?) = rooms[roomId]?.let {
        it.join(guest, password)
        it
    } ?: run {
        throw IllegalStateException("No roomId found $roomId")
    }

    fun getAll() = rooms.values.toList()
}
