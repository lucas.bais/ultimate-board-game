package com.butterandchildren.game_core.assets

import com.butterandchildren.game_core.game.component.Zone
import com.butterandchildren.game_core.game.objects.GameObject
import com.google.gson.Gson
import com.google.gson.GsonBuilder

object AssetRepository {

    private val gson: Gson = GsonBuilder()
        .registerTypeAdapter(GameObject::class.java, GameObjectDeserializer())
        .create()

    private val assets: Assets =
        gson.fromJson(this::class.java.classLoader.getResource("static_assets.json").readText(), Assets::class.java)

    // TODO Remove - hack for Seryo.
    fun get(id: String): Asset {
        val hackedZones = mutableListOf<Zone>()
        val ass = assets.copy()
        ass.assets.filter { it.startsShuffled }.forEach { asset ->
            asset.zones.forEach { zone ->
                hackedZones.add(zone.copy(objects = zone.objects.toMutableList()))
            }
        }

        hackedZones.forEach { it.objects.shuffle() }

        return ass.assets.first().copy(zones = hackedZones)
    }

    fun all(): List<Asset> = assets.assets
}
