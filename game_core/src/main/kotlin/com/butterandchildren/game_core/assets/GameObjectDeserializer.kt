package com.butterandchildren.game_core.assets

import com.butterandchildren.game_core.game.objects.GameObject
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type

class GameObjectDeserializer : JsonDeserializer<GameObject> {
    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): GameObject {
        json?.let {
            val gameObject = it.asJsonObject
            return GameObject.Card(
                gameObject.get("id").asString,
                if (gameObject.get("side").asString == GameObject.Card.Side.DOWN.name
                ) GameObject.Card.Side.DOWN else GameObject.Card.Side.UP,
                gameObject.get("imageUp").asString,
                gameObject.get("imageDown").asString
            )
        }

        throw IllegalStateException("GameObjectDeserializer error")
    }
}
