package com.butterandchildren.game_core.assets

data class Assets(
    val assets: List<Asset>
)
