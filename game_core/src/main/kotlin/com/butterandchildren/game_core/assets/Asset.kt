package com.butterandchildren.game_core.assets

import com.butterandchildren.game_core.game.component.Zone
import com.butterandchildren.game_core.game.objects.Board

data class Asset(
    val id: String,
    val name: String,
    val description: String,
    val zones: List<Zone>,
    val maxBoardZones: Int,
    val maxHandZones: Int,
    val startsShuffled: Boolean = false
) {
    fun board() = Board.create(maxBoardZones).zones
}
