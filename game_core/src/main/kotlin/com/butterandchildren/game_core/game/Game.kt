package com.butterandchildren.game_core.game

import com.butterandchildren.game_core.assets.Asset
import com.butterandchildren.game_core.game.action.Action
import com.butterandchildren.game_core.game.action.AddGameObject
import com.butterandchildren.game_core.game.action.DeleteGameObject
import com.butterandchildren.game_core.game.action.Move
import com.butterandchildren.game_core.game.action.Replace
import com.butterandchildren.game_core.game.action.Shuffle
import com.butterandchildren.game_core.game.action.TakeToHand
import com.butterandchildren.game_core.game.component.Zone
import com.butterandchildren.game_core.game.objects.GameObject
import java.util.LinkedList

class Game private constructor(initial: GameState) {

    companion object {
        fun newGame(room: Room, asset: Asset) = newGame(
            asset.board(), asset.zones,
            room.guests().map {
                Player.from(it, asset.maxHandZones)
            }
        )

        fun newGame(
            board: List<Zone>,
            assets: List<Zone>,
            players: List<Player>
        ): Game = Game(
            GameState(
                players = players,
                board = board,
                assets = assets
            )
        )
    }

    private val stateQueue = LinkedList<GameState>()

    init {
        stateQueue.add(initial)
    }

    val currentState: GameState
        get() = stateQueue.last

    fun execute(action: Action): GameState {
        stateQueue.add(mutate(currentState.copy(), action))
        return stateQueue.last
    }

    private fun mutate(newGameState: GameState, action: Action): GameState {
        when (action) {
            is DeleteGameObject -> {
                newGameState.removeObject(action.id)
            }
            is AddGameObject -> {
                newGameState.addObject(action.zoneId, action.gameObject)
            }
            is Replace -> {
                newGameState.replace(action.newGameObject)
            }
            is TakeToHand -> {
                val zone = newGameState.findZone(action.zoneId)
                if (zone.objects.size >= action.quantity) {
                    val pickedElements = mutableListOf<GameObject>()
                    for (e in 1..action.quantity) {
                        pickedElements.add(zone.objects.removeAt(0))
                    }
                    val newPlayerValue = newGameState.findPlayer(action.userId)
                        .expandToFit(pickedElements.size)
                        .spread(pickedElements)
                    return newGameState.replace(newPlayerValue)
                }
                throw IllegalStateException("can not take ${action.quantity} from ${action.zoneId}")
            }
            is Shuffle -> {
                newGameState.findZone(action.zoneId).objects.shuffle()
            }
            is Move -> {
                val targetObject = newGameState.findObject(objectId = action.objectId)
                newGameState.removeObject(action.objectId)
                newGameState.addObject(action.toZoneId, targetObject)
            }
        }

        return newGameState
    }
}
