package com.butterandchildren.game_core.game.objects

import com.butterandchildren.game_core.game.action.ObjectAction
import java.net.URI

sealed class GameObject(open val id: String) {

    abstract fun image(): URI

    abstract fun type(): String

    abstract fun actions(): Set<ObjectAction>

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as GameObject

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }

    data class Card(
        override val id: String,
        val side: Side = Side.DOWN,
        val imageUp: String,
        val imageDown: String
    ) : GameObject(id) {

        fun flip(): Card = Card(
            id, if (side == Side.UP) Side.DOWN else Side.UP,
            imageUp = imageUp,
            imageDown = imageDown
        )

        enum class Side {
            UP,
            DOWN
        }

        override fun image() =
            if (side == Side.UP) URI.create(imageUp)
            else URI.create(imageDown)

        override fun type(): String = "CARD"

        override fun actions(): Set<ObjectAction> = setOf(
            object : ObjectAction {
                override fun execute(): GameObject = flip()
                override fun name(): String = "flip"
            })
    }

    class Deck(id: String, cards: List<Card>) : GameObject(id) {

        companion object {

            private val EMPTY_DECK_ERROR = IllegalStateException("Can't draw a card of an empty deck")

            fun create(id: String, cards: List<Card>): Deck = Deck(id, cards)

//            fun spanishDeck(id: String): Deck {
//                val cards = mutableListOf<Card>()
//                for (number in IntRange(1, 12)) {
//                    for (palo in listOf("Basto", "Espada", "Oro", "Copa")) {
//                        cards.add(Card("$number - $palo"))
//                    }
//                }
//                cards.add(Card("Comodin 01"))
//                cards.add(Card("Comodin 02"))
//                return create(id, cards)
//            }

//            fun trucoDeck(id: String): Deck = spanishDeck(id).filter {
//                !it.id.contains("9") && !it.id.contains("8") && !it.id.contains("Comodin")
//            }
        }

        private val deckCards = cards.toMutableList()

        fun getCards() = deckCards.toList()

        fun isEmpty(): Boolean = deckCards.isEmpty()

        fun size() = deckCards.size

        fun filter(filter: (Card) -> Boolean): Deck {
            return Deck(id, deckCards.filter { filter(it) })
        }

        @Throws(IllegalStateException::class)
        fun pick(): Card {
            if (deckCards.isEmpty()) throw EMPTY_DECK_ERROR
            return deckCards.removeAt(0)
        }

        @Throws(IllegalStateException::class)
        fun peek(): Card {
            if (deckCards.isEmpty()) throw EMPTY_DECK_ERROR
            return deckCards[0]
        }

        fun shuffle(): Deck {
            deckCards.shuffle()
            return this
        }

        override fun toString(): String = "Deck($id=${deckCards.size})"

        override fun image(): URI = peek().image()

        override fun type(): String = Deck::class.java.name

        override fun actions(): Set<ObjectAction> = setOf(
            object : ObjectAction {
                override fun execute(): GameObject = shuffle()
                override fun name(): String = "shuffle"
            },
            object : ObjectAction {
                override fun execute(): GameObject = pick()
                override fun name(): String = "pick"
            },
            object : ObjectAction {
                override fun execute(): GameObject = peek()
                override fun name(): String = "peek"
            }
        )
    }
}
