package com.butterandchildren.game_core.game.component

data class Dimension(val width: Int, val height: Int)
