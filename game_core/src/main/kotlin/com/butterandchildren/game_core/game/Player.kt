package com.butterandchildren.game_core.game

import com.butterandchildren.game_core.game.component.Zone
import com.butterandchildren.game_core.game.objects.GameObject

fun Player.expandToFit(freeSpaces: Int): Player {
    val newHand = this.hand.toMutableList()
    val emptySpaces = newHand.count { it.objects.isEmpty() }
    val spacesToAdd = (freeSpaces + 1 - emptySpaces)
    var currentLast = newHand.size
    // create new hand zones needed to fill the free spaces needed.
    (spacesToAdd..0).forEach { _ ->
        currentLast++
        newHand.add(Zone("user-${this.id}--hand--$currentLast"))
    }
    return this.copy(hand = newHand)
}

fun Player.spread(objects: List<GameObject>): Player {
    val reminder = objects.toMutableList()
    this.hand.filter { it.objects.isEmpty() }.forEach {
        // TODO fix - horrible hack.
        if (reminder.size > 0)
            it.objects.add(reminder.removeAt(0))
    }
    // mutable objects sadly
    return this
}

data class Player(
    val id: Int,
    val name: String = id.toString(),
    val hand: List<Zone>
) {

    companion object {
        fun from(guest: Guest, maxHandAmount: Int) = Player(
            guest.id, guest.name,
            mutableListOf<Zone>().also { list ->
                for (number in 1..maxHandAmount) {
                    list.add(Zone("user-${guest.id}--hand--$number"))
                }
            }
        )
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Player

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }
}
