package com.butterandchildren.game_core.game

import com.butterandchildren.game_core.game.component.Zone
import com.butterandchildren.game_core.game.objects.GameObject

data class GameState(
    val board: List<Zone>,
    val assets: List<Zone>,
    val players: List<Player>
) {

    fun replace(newPlayer: Player): GameState {
        return this.copy(
            players = this.players.toMutableList().apply {
                replaceAll { player ->
                    if (player.id == newPlayer.id) newPlayer else player
                }
            }
        )
    }

    fun replace(newGameObject: GameObject) {
        board.find { zone ->
            zone.objects.any { it.id == newGameObject.id }
        }?.replace(newGameObject)

        players.forEach { player ->
            player.hand.find { zone ->
                zone.objects.any { it.id == newGameObject.id }
            }?.replace(newGameObject)
        }
    }

    fun findPlayer(playerId: Int): Player {
        return players.find { it.id == playerId } ?: throw IllegalStateException("Can't find player")
    }

    fun findZone(zoneId: String): Zone {
        board.find { zone -> zone.id == zoneId }?.let {
            return it
        }
        assets.find { zone -> zone.id == zoneId }?.let {
            return it
        }
        players.forEach { player ->
            player.hand.find { zone -> zone.id == zoneId }?.let {
                return it
            }
        }
        throw IllegalStateException("Can't find $zoneId")
    }

    fun findObject(objectId: String): GameObject {
        val elementInBoard = board.find { zone ->
            zone.objects.any { it.id == objectId }
        }?.getElementById(objectId) ?: run {
            assets.find { zone ->
                zone.objects.any { it.id == objectId }
            }?.getElementById(objectId)
        }

        if (elementInBoard == null) {
            players.forEach { player ->
                val gameObjectInHand = player.hand.find { zone ->
                    zone.objects.any { it.id == objectId }
                }?.getElementById(objectId)
                if (gameObjectInHand != null) return gameObjectInHand
            }
            throw IllegalStateException("Couldn't found on board or players hand $objectId")
        }
        return elementInBoard
    }

    fun isPlayer(zoneId: String): Boolean {
        players.forEach { player ->
            player.hand.find { zone -> zone.id == zoneId }?.let {
                return true
            }
        }
        return false
    }

    fun removeObject(objectId: String) {
        // remove game Object from board
        board.find { zone ->
            zone.objects.any { it.id == objectId }
        }?.also { zone ->
            zone.objects.remove(zone.objects.find { it.id == objectId })
        }

        assets.find { zone ->
            zone.objects.any { it.id == objectId }
        }?.also { zone ->
            zone.objects.remove(zone.objects.find { it.id == objectId })
        }

        // remove game Object from player
        players.forEach { player ->
            player.hand.find { zone ->
                zone.objects.any { it.id == objectId }
            }?.also { zone ->
                zone.objects.remove(zone.objects.find { it.id == objectId })
            }
        }
    }

    fun addObject(zoneId: String, gameObject: GameObject) {
        if (gameObject is GameObject.Card && isPlayer(zoneId)) {
            findZone(zoneId).objects.add(gameObject.copy(side = GameObject.Card.Side.UP))
        } else {
            findZone(zoneId).objects.add(gameObject)
        }
    }
}
