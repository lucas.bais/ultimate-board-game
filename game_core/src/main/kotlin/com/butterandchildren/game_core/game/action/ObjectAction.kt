package com.butterandchildren.game_core.game.action

import com.butterandchildren.game_core.game.objects.GameObject

interface ObjectAction {
    fun execute(): GameObject
    fun name(): String
}
