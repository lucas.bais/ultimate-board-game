package com.butterandchildren.game_core.game.objects

import com.butterandchildren.game_core.game.component.Zone

data class Board(val zones: List<Zone>) {
    companion object {
        fun create(zones: Int): Board {
            return Board(
                mutableListOf<Zone>().also {
                    for (value in 1..zones) {
                        it.add(Zone("board__$value"))
                    }
                }
            )
        }
    }
}
