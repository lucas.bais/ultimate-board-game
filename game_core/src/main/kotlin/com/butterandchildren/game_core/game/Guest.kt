package com.butterandchildren.game_core.game

data class Guest(val id: Int, val name: String = id.toString())
