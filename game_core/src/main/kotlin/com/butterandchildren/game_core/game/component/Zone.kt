package com.butterandchildren.game_core.game.component

import com.butterandchildren.game_core.game.objects.GameObject

data class Zone(
    val id: String,
    val objects: MutableList<GameObject> = mutableListOf(),
    val dimension: Dimension = Dimension(1, 1)
) {

    // TODO create factory method in order to transform it into a data class.
    // TODO Make zone non-mutable

    fun getElementById(id: String) = objects.find {
        it.id == id
    }

    fun replace(newGameObject: GameObject) {
        val index = objects.indexOfFirst { it.id == newGameObject.id }
        objects[index] = newGameObject
    }
}
