package com.butterandchildren.game_core.game

// TODO: maxParticipants
// TODO: createdAt
class Room(
    private val ownerId: Int,
    val id: String,
    private val password: String? = null,
    val name: String
) {

    val guests = mutableSetOf<Guest>()

    @Suppress("unused")
    val isPublic = password.isNullOrEmpty()

    fun join(guest: Guest, password: String?) {
        if (guest.id == ownerId || this.password.isNullOrEmpty() || this.password == password)
            guests.add(guest)
        else
            throw IllegalArgumentException("invalid room password.")
    }

    fun guests() = guests.toList()

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Room

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }
}
