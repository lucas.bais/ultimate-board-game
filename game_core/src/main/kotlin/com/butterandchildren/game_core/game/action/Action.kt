package com.butterandchildren.game_core.game.action

import com.butterandchildren.game_core.game.objects.GameObject

sealed class Action

data class Shuffle(val zoneId: String) : Action()

data class DeleteGameObject(val id: String) : Action()

data class Replace(val newGameObject: GameObject) : Action()

data class AddGameObject(val gameObject: GameObject, val zoneId: String) : Action()

data class Move(val objectId: String, val fromZoneId: String, val toZoneId: String) : Action()

data class TakeToHand(
    val userId: Int,
    val zoneId: String,
    val quantity: Int
) : Action()
