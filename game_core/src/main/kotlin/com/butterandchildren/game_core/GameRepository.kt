package com.butterandchildren.game_core

import com.butterandchildren.game_core.assets.AssetRepository
import com.butterandchildren.game_core.game.Game
import com.butterandchildren.game_core.game.Room
import java.util.concurrent.ConcurrentHashMap

object GameRepository {

    private val roomRepository: RoomRepository = RoomRepository

    private val assetRepository: AssetRepository = AssetRepository

    private val games = ConcurrentHashMap<Room, Game>()

    // TODO separate start game  - restart logic.
    fun startGame(roomId: String, assetId: String): Game {
        val room = roomRepository.getRoom(roomId)
        // TODO hack to restart game.
        // TODO move this.
        // TODO replace with original well planned behaviour
//        if (games[room] != null)
//            throw IllegalStateException("The roomId $roomId has an open game already, you can't start it again")
        val assets = assetRepository.get(assetId)
        val game = Game.newGame(room, assets)
        games[room] = game
        return game
    }

    fun getGame(roomId: String): Game {
        val room = roomRepository.getRoom(roomId)
        val game = games[room]
        if (game != null)
            return game
        else throw IllegalStateException("There is no game for roomId $roomId")
    }

    fun getAll() = games.values.toList()
}
