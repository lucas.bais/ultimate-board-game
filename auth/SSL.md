

Generate a 2048-bit RSA private key

$ openssl genrsa -out private_key.pem 2048

Convert private Key to PKCS#8 format (so Java can read it)

$ openssl pkcs8 -topk8 -inform PEM -outform DER -in private_key.pem -out private_key.der -nocrypt

Output public key portion in DER format (so Java can read it)

$ openssl rsa -in private_key.pem -pubout -outform DER -out public_key.der



export DOMAIN=181.170.176.207
export EMAIL=lucas.bais@gmail.com
export PORT=8889
export ALIAS=ugbx
certbot certonly -n -d $DOMAIN --email "$EMAIL" --agree-tos --standalone --preferred-challenges http --http-01-port $PORT



## IMPORTANT NOTES:
    - Congratulations! Your certificate and chain have been saved at:
      /etc/letsencrypt/live/api.ugbx.online/fullchain.pem
      Your key file has been saved at:
      /etc/letsencrypt/live/api.ugbx.online/privkey.pem
      Your cert will expire on 2021-02-13. To obtain a new or tweaked
      version of this certificate in the future, simply run certbot
      again. To non-interactively renew *all* of your certificates, run
      "certbot renew"
    - If you like Certbot, please consider supporting our work by:
   
      Donating to ISRG / Let's Encrypt:   https://letsencrypt.org/donate
      Donating to EFF:                    https://eff.org/donate-le
      
      
      
export DOMAIN=my.example.com
export EMAIL=root@example.com
export PORT=8889
export ALIAS=myalias
certbot certonly -n -d $DOMAIN --email "$EMAIL" --agree-tos --standalone --preferred-challenges http --http-01-port $PORT

set PASSWD="demo_password"
export ALIAS=client
export DOMAIN=api.ugbx.online

openssl pkcs12 -export -out /etc/letsencrypt/live/$DOMAIN/keystore.p12 -inkey /etc/letsencrypt/live/$DOMAIN/privkey.pem -in /etc/letsencrypt/live/$DOMAIN/fullchain.pem -name $ALIAS

# Generate a PKCS12 certificate bundle from the cert and private key
openssl pkcs12 -export -out demo_keystore.p12 -inkey privkey.pem -in fullchain.pem -name $ALIAS \
-passin pass:$PASSWD -passout pass:$PASSWD

# Convert to BKS
keytool -importkeystore -alias $ALIAS -srckeystore demo_keystore.p12 -srcstoretype PKCS12 \
  -srcstorepass $PASSWD -storepass $PASSWD \
  -deststoretype BKS -providerpath bcprov-jdk15on-166.jar \
  -provider org.bouncycastle.jce.provider.BouncyCastleProvider -destkeystore demo_keystore.bks
