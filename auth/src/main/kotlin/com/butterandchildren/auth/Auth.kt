package com.butterandchildren.auth

import com.butterandchildren.auth.model.Credentials
import com.butterandchildren.auth.model.UserCredentials
import com.butterandchildren.auth.storage.UserStorageService
import io.ktor.http.HttpStatusCode
import io.ktor.server.application.Application
import io.ktor.server.application.call
import io.ktor.server.application.install
import io.ktor.server.auth.Authentication
import io.ktor.server.auth.authenticate
import io.ktor.server.auth.authentication
import io.ktor.server.auth.jwt.jwt
import io.ktor.server.request.receive
import io.ktor.server.response.respond
import io.ktor.server.routing.Route
import io.ktor.server.routing.Routing
import io.ktor.server.routing.get
import io.ktor.server.routing.post
import io.ktor.server.routing.route
import io.ktor.server.routing.routing

fun Application.butterAuthentication() {
    val userRepository = UserStorageService()
    val authenticationTokenService = AuthenticationTokenService()
    val authenticationService = AuthenticationService(authenticationTokenService, userRepository)

    install(Authentication) {
        jwt {
            realm = authenticationTokenService.getRealm()
            verifier(authenticationTokenService)
            validate { credential ->
                val payload = credential.payload
                val claim = payload.getClaim("id")
                val userId = claim.asInt()
                val user = userRepository.findUser(userId)
                user
            }
        }
    }
    routing {
        auth(authenticationService)
        user()
    }
}

private fun Routing.user() {
    authenticate {
        route("user") {
            get("/") {
                (call.authentication.principal as? UserCredentials)?.let {
                    call.respond(HttpStatusCode.OK, mapOf("userId" to it.userId))
                } ?: run {
                    throw IllegalStateException("Couldn't process user")
                }
            }
        }
    }
}

private fun Route.auth(authenticationService: AuthenticationService) {
    route("auth") {
        post("sign_up") {
            val credentials = call.receive<Credentials>()
            call.respond(HttpStatusCode.OK, authenticationService.signUp(credentials))
        }

        post("sign_in") {
            val credentials = call.receive<Credentials>()
            call.respond(HttpStatusCode.OK, authenticationService.signIn(credentials))
        }
    }
}
