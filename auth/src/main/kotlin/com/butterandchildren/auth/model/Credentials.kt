package com.butterandchildren.auth.model

import io.ktor.server.auth.Principal
import java.io.Serializable

internal data class AuthResponse(val userId: Int, val accessToken: String)

internal data class Credentials(val email: String, val password: String) {

    override fun toString(): String {
        // doing this so we never risk logging the password
        return "Credentials(email=$email, password=****)"
    }
}

data class UserCredentials(val userId: Int, val email: String, val password: ByteArray) :
    Serializable,
    Principal {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as UserCredentials

        if (userId != other.userId) return false
        if (email != other.email) return false
        if (!password.contentEquals(other.password)) return false

        return true
    }

    override fun toString(): String {
        // doing this so we never risk logging the password
        return "UserCredentials(userId=$userId, email=$email,password=****)"
    }

    override fun hashCode(): Int {
        var result = userId
        result = 31 * result + email.hashCode()
        result = 31 * result + password.contentHashCode()
        return result
    }
}
