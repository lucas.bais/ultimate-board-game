package com.butterandchildren.auth.model

import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Table

internal object Users : Table() {
    val userId: Column<Int> = integer("id").autoIncrement()
    val email = varchar("email", 128).uniqueIndex()
    val passwordHash = binary("password_hash", 64)
    override val primaryKey = PrimaryKey(userId, name = "PK_Users_ID")
}
