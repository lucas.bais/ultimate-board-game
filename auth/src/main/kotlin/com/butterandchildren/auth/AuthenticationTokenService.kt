package com.butterandchildren.auth

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import com.auth0.jwt.interfaces.DecodedJWT
import com.auth0.jwt.interfaces.JWTVerifier
import java.util.Date

internal class AuthenticationTokenService : JWTVerifier {

    // TODO env.
    private val jwtIssuer = "https://www.ugbx.com/"
    private val jwtAudience = "ugbx-users"
    private val jwtRealm = "ugbx-clients"

    private val jwtAlgorithm = Algorithm.HMAC256(System.getenv("HMAC_PSEUDO"))

    private val verifier: JWTVerifier = JWT
        .require(jwtAlgorithm)
        .withIssuer(jwtIssuer)
        .withAudience(jwtAudience)
        .build()

    fun generateAuthToken(userId: Int): String = JWT.create()
        .withIssuer(jwtIssuer)
        .withAudience(jwtAudience)
        .withSubject("Authentication")
        .withClaim("id", userId)
        .withExpiresAt(expiresAt())
        .sign(jwtAlgorithm)

    private fun expiresAt() = Date(System.currentTimeMillis() + 3_600_000 * 24)

    override fun verify(token: String?): DecodedJWT {
        return verifier.verify(token)
    }

    override fun verify(jwt: DecodedJWT?): DecodedJWT {
        return verifier.verify(jwt)
    }

    fun getRealm(): String = jwtRealm
}
