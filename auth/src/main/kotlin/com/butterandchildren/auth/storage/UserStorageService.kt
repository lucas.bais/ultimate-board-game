package com.butterandchildren.auth.storage

import com.butterandchildren.auth.UserRepository
import com.butterandchildren.auth.UserRepository.Companion.digestFunction
import com.butterandchildren.auth.model.Credentials
import com.butterandchildren.auth.model.UserCredentials
import com.butterandchildren.auth.model.Users
import com.butterandchildren.auth.storage.DatabaseFactory.dbQuery
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.statements.InsertStatement

internal class UserStorageService : UserRepository {

    init {
        DatabaseFactory.init()
    }

    override suspend fun addUser(credentials: Credentials): UserCredentials? {
        var statement: InsertStatement<Number>? = null
        dbQuery {
            statement = Users.insert { user ->
                user[email] = credentials.email
                user[passwordHash] = digestFunction(credentials.password)
            }
        }
        return rowToUser(statement?.resultedValues?.get(0))
    }

    private fun rowToUser(row: ResultRow?): UserCredentials? {
        if (row == null) {
            return null
        }
        return UserCredentials(
            userId = row[Users.userId],
            email = row[Users.email],
            password = row[Users.passwordHash]
        )
    }

    override suspend fun findUser(userId: Int): UserCredentials? = dbQuery {
        Users.select { Users.userId.eq(userId) }
            .map { rowToUser(it) }.singleOrNull()
    }

    override suspend fun findUserByEmail(email: String): UserCredentials? = dbQuery {
        Users.select { Users.email.eq(email) }
            .map { rowToUser(it) }.singleOrNull()
    }
}
