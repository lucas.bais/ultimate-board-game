package com.butterandchildren.auth

import com.butterandchildren.auth.model.Credentials
import com.butterandchildren.auth.model.UserCredentials
import io.ktor.util.getDigestFunction

internal interface UserRepository {
    companion object {

        val digestFunction = getDigestFunction("SHA-256") {
            System.getenv("SHA-256-SALT")
        }
    }

    suspend fun addUser(credentials: Credentials): UserCredentials?

    suspend fun findUser(userId: Int): UserCredentials?

    suspend fun findUserByEmail(email: String): UserCredentials?
}
