package com.butterandchildren.auth

import com.butterandchildren.auth.UserRepository.Companion.digestFunction
import com.butterandchildren.auth.model.AuthResponse
import com.butterandchildren.auth.model.Credentials
import java.security.MessageDigest

internal class AuthenticationService(
    private val authenticationTokenService: AuthenticationTokenService,
    private val userRepository: UserRepository
) {

    suspend fun signIn(credentials: Credentials): AuthResponse =
        userRepository.findUserByEmail(credentials.email)?.let { authCredentialInfo ->
            if (MessageDigest.isEqual(authCredentialInfo.password, digestFunction(credentials.password)))
                return AuthResponse(
                    authCredentialInfo.userId,
                    authenticationTokenService.generateAuthToken(userId = authCredentialInfo.userId)
                )
            throw IllegalStateException("Invalid credentials for ${credentials.email}")
        } ?: run {
            throw IllegalStateException("Invalid user or credentials for ${credentials.email}")
        }

    suspend fun signUp(credentials: Credentials): AuthResponse =
        userRepository.findUserByEmail(credentials.email)?.let {
            throw IllegalStateException("Credential already exists for email=${credentials.email}")
        } ?: run {
            userRepository.addUser(credentials)?.let {
                return AuthResponse(it.userId, authenticationTokenService.generateAuthToken(it.userId))
            } ?: run {
                throw IllegalStateException("Error creating user for email=${credentials.email}")
            }
        }
}
