# Created by Butter and Children (c)

## Server

Made in kotlin.

Uses [Ktor](https://ktor.io/) as async web framework.

- open with Intellij
- run `com/butterandchildren/Application.kt`
- starts the server running on `:8080`

- You must request the following environment variables:
```bash
SHA-256-SALT=
DATABASE_URL_JDBC=
DATABASE_USER=
DATABASE_PASSWORD=
HMAC_PSEUDO=
```

## Create a user 

```bash
curl -d '{"name":"USER"}' -H "Content-Type: application/json" -X POST http://0.0.0.0:8080/users
```

## Create a room (userId==player id)

```bash
curl -d '{"userId":"USER_ID"}' -H "Content-Type: application/json" -X POST http://0.0.0.0:8080/rooms
```

## Join a room (id==player id)

```bash
curl -d '{"id":"USER_ID"}' -H "Content-Type: application/json" -X PUT http://0.0.0.0:8080/rooms/{room_id}
```

## Start a game (id==room id)

```bash
curl -d '{"id":"0"}' -H "Content-Type: application/json" -X POST http://0.0.0.0:8080/games/
```

## Operations to make
- Take to hand takes last one not first.
- Take to hand flips it