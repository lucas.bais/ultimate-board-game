package com.butterandchildren

import com.butterandchildren.auth.butterAuthentication
import com.butterandchildren.game_core.assets.AssetRepository
import com.butterandchildren.game_core.game.objects.GameObject
import com.butterandchildren.response.GameObjectDeserializer
import com.butterandchildren.response.GameObjectSerializer
import com.butterandchildren.routes.games
import com.butterandchildren.routes.objects
import com.butterandchildren.routes.rooms
import com.butterandchildren.routes.zones
import com.fasterxml.jackson.databind.module.SimpleModule
import io.ktor.http.ContentType
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.serialization.jackson.jackson
import io.ktor.server.application.Application
import io.ktor.server.application.call
import io.ktor.server.application.install
import io.ktor.server.http.content.resources
import io.ktor.server.http.content.static
import io.ktor.server.plugins.autohead.AutoHeadResponse
import io.ktor.server.plugins.callloging.CallLogging
import io.ktor.server.plugins.contentnegotiation.ContentNegotiation
import io.ktor.server.plugins.cors.routing.CORS
import io.ktor.server.plugins.defaultheaders.DefaultHeaders
import io.ktor.server.plugins.statuspages.StatusPages
import io.ktor.server.response.respond
import io.ktor.server.response.respondText
import io.ktor.server.routing.get
import io.ktor.server.routing.routing

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused") // Referenced in application.conf
@JvmOverloads
fun Application.module(testing: Boolean = false) {
    install(AutoHeadResponse)
    install(DefaultHeaders)
    install(CallLogging)

    install(ContentNegotiation) {
        jackson {
            registerModule(
                SimpleModule().also {
                    it.addSerializer(GameObject::class.java, GameObjectSerializer())
                    it.addDeserializer(GameObject::class.java, GameObjectDeserializer())
                }
            )
        }
    }

    install(StatusPages) {

        exception<IllegalArgumentException> { call, cause ->
            call.respond(
                HttpStatusCode.Forbidden,
                mapOf(
                    "error" to (cause.message ?: ""),
                    "errorMessage" to (cause.message ?: "")
                )
            )
        }

        exception<IllegalStateException> { call, cause ->
            call.respond(
                HttpStatusCode.Forbidden,
                mapOf(
                    "error" to (cause.message ?: ""),
                    "errorMessage" to (cause.message ?: "")
                )
            )
        }
    }

    butterAuthentication()

    routing {

        /**
         * Server health check method.
         */
        get("/ping") {
            call.respondText("pong", contentType = ContentType.Text.Plain)
        }

        // Static files
        static("static") {
            resources("cards/spanish_cards")
            resources("cards/tarot_cards")
            resources("cards/poker_cards")
        }

        /**
         * Returns a list of active rooms.
         */
        get("/assets") {
            call.respond(HttpStatusCode.OK, AssetRepository.all())
        }

        rooms()
        zones()
        games()
        objects()
    }

    install(CORS) {
        allowHeader(HttpHeaders.Authorization)
        allowMethod(HttpMethod.Options)
        allowMethod(HttpMethod.Get)
        allowMethod(HttpMethod.Post)
        allowMethod(HttpMethod.Put)
        allowMethod(HttpMethod.Delete)
        allowMethod(HttpMethod.Patch)
        allowCredentials = true
        allowNonSimpleContentTypes = true
        anyHost()
    }
}
