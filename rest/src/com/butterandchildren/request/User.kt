package com.butterandchildren.request

data class User(val id: String, val name: String)
