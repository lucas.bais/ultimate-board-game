package com.butterandchildren.request

data class TakeRequest(
    val zoneId: String,
    val quantity: Int
)
