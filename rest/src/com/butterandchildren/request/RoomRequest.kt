package com.butterandchildren.request

data class RoomRequest(
    val name: String? = null,
    val password: String? = null
)
