package com.butterandchildren.request

data class MoveRequest(
    val zoneId: String,
    val destinationZoneId: String,
    val targetObjectId: String
)
