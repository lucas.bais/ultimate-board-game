package com.butterandchildren.request

class GameRequest(
    id: String,
    val assetId: String = "asset_dixit"
) : Id(id)
