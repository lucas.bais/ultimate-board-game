package com.butterandchildren.routes

import com.butterandchildren.game_core.GameRepository
import com.butterandchildren.game_core.game.action.DeleteGameObject
import com.butterandchildren.game_core.game.action.Replace
import io.ktor.http.HttpStatusCode
import io.ktor.server.application.call
import io.ktor.server.auth.authenticate
import io.ktor.server.response.respond
import io.ktor.server.routing.Routing
import io.ktor.server.routing.delete
import io.ktor.server.routing.get
import io.ktor.server.routing.put

fun Routing.objects() {
    /**
     * Get object content.
     * ObjectId must exist in player's hand or board.
     */
    get("/games/{game_id}/objects/{object_id}") {
        val gameId = call.parameters["game_id"] as String
        val objectId = call.parameters["object_id"] as String
        call.respond(
            HttpStatusCode.OK,
            GameRepository.getGame(
                gameId
            ).currentState.findObject(objectId)
        )
    }

    authenticate {

        /**
         * Given an active game id and an object id delete the object if it exists in the game.
         *
         * @returns deleted object as json
         */
        delete("/games/{game_id}/objects/{object_id}") {
            val gameId = call.parameters["game_id"] as String
            val objectId = call.parameters["object_id"] as String
            val gameObject = GameRepository.getGame(gameId).currentState.findObject(objectId)
            GameRepository.getGame(gameId)
                .execute(DeleteGameObject(objectId))
            call.respond(HttpStatusCode.OK, gameObject)
        }

        /**
         * Well defined action of flip. Flip the object if the action is defined.
         * @returns gameState as json
         */
        put("/games/{game_id}/objects/{object_id}/flip") {
            val gameId = call.parameters["game_id"] as String
            val objectId = call.parameters["object_id"] as String
            val game = GameRepository.getGame(gameId)
            val gameObject = game.currentState.findObject(objectId)

            // TODO - Current replace action mutates the current state. Does not create a new one.
            gameObject.actions().find {
                it.name() == "flip"
            }?.execute()?.also {
                game.execute(Replace(it))
            }
            call.respond(HttpStatusCode.OK, game.currentState)
        }
    }
}
