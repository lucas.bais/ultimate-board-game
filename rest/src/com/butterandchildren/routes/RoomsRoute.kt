package com.butterandchildren.routes

import com.butterandchildren.auth.model.UserCredentials
import com.butterandchildren.game_core.RoomRepository
import com.butterandchildren.game_core.game.Guest
import com.butterandchildren.request.JoinRoomRequest
import com.butterandchildren.request.RoomRequest
import io.ktor.http.HttpStatusCode
import io.ktor.server.application.call
import io.ktor.server.auth.authenticate
import io.ktor.server.auth.authentication
import io.ktor.server.request.receive
import io.ktor.server.response.respond
import io.ktor.server.routing.Route
import io.ktor.server.routing.get
import io.ktor.server.routing.post
import io.ktor.server.routing.put

fun Route.rooms() {

    /**
     * Returns a list of active rooms.
     */
    get("/rooms") {
        call.respond(HttpStatusCode.OK, RoomRepository.getAll())
    }

    /**
     * Given a {room_id} returns the current room state.
     * @returns room instance.
     */
    get("/rooms/{room_id}") {
        val roomId = call.parameters["room_id"] as String
        call.respond(HttpStatusCode.OK, RoomRepository.getRoom(roomId))
    }

    authenticate {
        /**
         * Creates a room. Properties of the room are represented as RoomRequest.
         * (Player owner, name of the room, password)
         */
        post("/rooms") {
            val roomRequest = call.receive<RoomRequest>()

            (call.authentication.principal as? UserCredentials)?.let {
                call.respond(
                    HttpStatusCode.OK,
                    RoomRepository.createRoom(
                        it.userId,
                        roomRequest.name,
                        roomRequest.password
                    )
                )
            }
        }

        /**
         * Modifies {room_id}
         * When a Player is received, then adds it to the game
         * @returns room instance.
         */
        put("/rooms/{room_id}/join") {
            val roomId = call.parameters["room_id"] as String
            val joinRoomRequest = call.receive<JoinRoomRequest>()
            (call.authentication.principal as? UserCredentials)?.let {
                val room = RoomRepository.join(roomId, Guest(it.userId, it.email), joinRoomRequest.password)
                call.respond(HttpStatusCode.OK, room)
            }
        }
    }
}
