package com.butterandchildren.routes

import com.butterandchildren.auth.model.UserCredentials
import com.butterandchildren.game_core.GameRepository
import com.butterandchildren.game_core.game.action.AddGameObject
import com.butterandchildren.game_core.game.action.Move
import com.butterandchildren.game_core.game.action.Shuffle
import com.butterandchildren.game_core.game.action.TakeToHand
import com.butterandchildren.game_core.game.objects.GameObject
import com.butterandchildren.request.MoveRequest
import com.butterandchildren.request.TakeRequest
import io.ktor.http.HttpStatusCode
import io.ktor.server.application.call
import io.ktor.server.auth.authenticate
import io.ktor.server.auth.authentication
import io.ktor.server.request.receive
import io.ktor.server.response.respond
import io.ktor.server.routing.Route
import io.ktor.server.routing.post
import io.ktor.server.routing.put

fun Route.zones() {

    authenticate {

        /**
         * Add object into certain area.
         * The zoneId must exist in player's hand or board.
         */
        post("/games/{game_id}/zones/{zone_id}/objects") {
            val gameId = call.parameters["game_id"] as String
            val zoneId = call.parameters["zone_id"] as String
            val gameObject = call.receive<GameObject>()
            call.respond(
                HttpStatusCode.OK,
                GameRepository.getGame(
                    gameId
                ).execute(
                    AddGameObject(
                        gameObject,
                        zoneId
                    )
                )
            )
        }

        /**
         * shuffle zone objects.
         * @returns Game state
         */
        put("/games/{game_id}/zones/{zone_id}/shuffle") {
            val gameId = call.parameters["game_id"] as String
            val zoneId = call.parameters["zone_id"] as String
            call.respond(
                HttpStatusCode.OK,
                GameRepository.getGame(
                    gameId
                ).execute(Shuffle(zoneId))
            )
        }

        /**
         * Take (X) amount from "top" and move it to the first empty slot in your hand.
         * @returns Game state
         */
        put("/games/{game_id}/zones/{zone_id}/take") {
            (call.authentication.principal as? UserCredentials)?.let {
                val gameId = call.parameters["game_id"] as String
                val zoneId = call.parameters["zone_id"] as String
                val quantity = call.receive<TakeRequest>()
                val game = GameRepository.getGame(gameId)
                game.execute(
                    TakeToHand(
                        it.userId,
                        zoneId,
                        quantity.quantity
                    )
                )
                call.respond(HttpStatusCode.OK, game.currentState)
            }
        }

        /**
         * Move one object in certain zone to another.
         * @returns Game state
         */
        put("/games/{game_id}/zones/{zone_id}/move_object") {
            (call.authentication.principal as? UserCredentials)?.let {
                val gameId = call.parameters["game_id"] as String
                val zoneId = call.parameters["zone_id"] as String
                val moveRequest = call.receive<MoveRequest>()
                val game = GameRepository.getGame(gameId)
                game.execute(
                    Move(
                        fromZoneId = zoneId,
                        toZoneId = moveRequest.destinationZoneId,
                        objectId = moveRequest.targetObjectId
                    )
                )
                call.respond(HttpStatusCode.OK, game.currentState)
            }
        }
    }
}
