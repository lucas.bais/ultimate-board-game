package com.butterandchildren.routes

import com.butterandchildren.game_core.GameRepository
import com.butterandchildren.request.GameRequest
import io.ktor.http.HttpStatusCode
import io.ktor.server.application.call
import io.ktor.server.auth.authenticate
import io.ktor.server.request.receive
import io.ktor.server.response.respond
import io.ktor.server.routing.Routing
import io.ktor.server.routing.get
import io.ktor.server.routing.post

fun Routing.games() {

    /**
     * Returns all active games.
     * @returns list of active games as json.
     */
    get("/games") {
        call.respond(
            HttpStatusCode.OK,
            GameRepository.getAll()
                .map { it.currentState }
        )
    }

    /**
     * Given a {game_id} returns a game
     * @returns game as json
     */
    get("/games/{game_id}") {
        val gameId = call.parameters["game_id"] as String
        call.respond(
            HttpStatusCode.OK,
            GameRepository.getGame(
                gameId
            ).currentState
        )
    }

    authenticate {
        /**
         * Creates a Game.
         * Creates a game given a roomId, returns the current game state.
         * @returns game state as json
         */
        post("/games") {
            val postData = call.receive<GameRequest>()
            GameRepository.startGame(
                roomId = postData.id,
                assetId = postData.assetId
            ).also {
                call.respond(HttpStatusCode.Created, it.currentState)
            }
        }
    }
}
