package com.butterandchildren.response

import com.butterandchildren.game_core.game.objects.GameObject
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import java.io.IOException

class GameObjectDeserializer @JvmOverloads constructor(t: Class<GameObject>? = null) : StdDeserializer<GameObject?>(t) {

    @Throws(IOException::class)
    override fun deserialize(p: JsonParser?, ctxt: DeserializationContext?): GameObject? {
        val node: JsonNode = p?.codec?.readTree(p)!!
        val id = node.get("id").asText()
        return when (node.get("type").asText()) {
            "CARD" -> {
                // TODO fix - create card deserializer
                val imageUp = node.get("imageUp").asText()
                val imageDown = node.get("imageDown").asText()
                val side = node.get("side").asText()
                GameObject.Card(
                    id,
                    if (side == GameObject.Card.Side.DOWN.name) GameObject.Card.Side.DOWN else GameObject.Card.Side.UP,
                    imageUp,
                    imageDown
                )
            }
            else -> null
        }
    }
}
