package com.butterandchildren.response

import com.butterandchildren.game_core.game.objects.GameObject
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import java.io.IOException

class GameObjectSerializer @JvmOverloads constructor(t: Class<GameObject>? = null) : StdSerializer<GameObject>(t) {

    @Throws(IOException::class)
    override fun serialize(
        value: GameObject,
        jgen: JsonGenerator,
        provider: SerializerProvider
    ) {
        jgen.writeStartObject()
        jgen.writeStringField("id", value.id)
        jgen.writeStringField("image", value.image().toString())
        jgen.writeStringField("type", value.type())
        when (value) {
            is GameObject.Card -> {
                jgen.writeStringField("imageUp", value.imageUp)
                jgen.writeStringField("imageDown", value.imageDown)
                jgen.writeStringField("side", value.side.name)
                // TODO actions
            }
            is GameObject.Deck -> {
                jgen.writeFieldName("cards/spanish_cards")
                jgen.writeStartArray()
                for (card in value.getCards()) {
                    jgen.writeObject(card)
                }
                jgen.writeEndArray()
                // TODO actions
            }
        }
        jgen.writeEndObject()
    }
}
